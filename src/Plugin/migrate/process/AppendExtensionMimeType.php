<?php

namespace Drupal\migrate_file_mime_extensions\Plugin\migrate\process;

use Drupal\Core\File\FileSystemInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Dflydev\ApacheMimeTypes\PhpRepository;

/**
 * If the file extension is missing, appends extension based on mime type.
 *
 * Available configuration keys:
 * - mime_mappings (optional): Map mime type to extension, keyed by mime type.
 *
 * If mime type is valid
 *   And the file's extension is valid for the mime type
 *     Do nothing.
 *   Else (if the file's extension is invalid)
 *     But an explicit mapping is provided
 *       Use extension mapped to mime type.
 *     Else (if an explicit mapping is not provided)
 *       Then get the list of valid extensions for the mime type and use the
 *       first one.
 * Else (if mime type is invalid)
 *   If the file should be deleted
 *     Remove it from the file system.
 *   Throw MigrateSkipRowException.
 *
 * Examples:
 *
 * @code
 * process:
 *   file:
 *     plugin: append_extension_mime_type
 *     source: fileurl
 *     mime_mappings:
 *       - 'image/jpeg'
 *       - 'image/png'
 *       - 'image/gif'
 * @endcode
 * The above example will transform a file path to include an extension based on
 * the mime type of the file, assuming the file does not already have a valid
 * extension given the mime type.
 *
 * @code
 * process:
 *   file:
 *     plugin: append_extension_mime_type
 *     source: fileurl
 *     mime_mappings:
 *       'image/jpeg': 'jpeg'
 *       'image/png': 'png'
 * @endcode
 * The above example will transform a file path to include the explicitly listed
 * extension if the file is of that mime type. However, if that file already has
 * a valid extension given the mime type, the existing extension will remain.
 *
 * @MigrateProcessPlugin(
 *   id = "append_extension_mime_type"
 * )
 */
class AppendExtensionMimeType extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Whether or not the download/copy should be deleted if it is invalid.
   *
   * Defaults to TRUE.
   *
   * @var bool
   */
  protected $cleanDownload;

  /**
   * Whether or not all mime types are supported.
   *
   * @var bool
   */
  protected $allMimeTypesSupported;

  /**
   * The available mime type extension mappings, if provided.
   *
   * If just a sequential list of types is provided in configuration, then this
   * mapping will contain the first valid extension from the mime types
   * extension repository.
   *
   * @var array
   *   Empty if no explicit list provided, otherwise it is an associated array
   *   of mime types mapped to a valid file extension for that mime type.
   */
  protected $mimeTypeMappings;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $file_system) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileSystem = $file_system;
    $this->cleanDownload = TRUE;
    if (isset($configuration['clean_download']) && $configuration['clean_download'] == FALSE) {
      $this->cleanDownload = FALSE;
    }
    $this->allMimeTypesSupported = TRUE;
    $this->mimeTypeMappings = [];
    if (!empty($configuration['mime_mappings']) && is_array($configuration['mime_mappings'])) {
      $this->allMimeTypesSupported = FALSE;
      $mappings = $this->configuration['mime_mappings'];
      $keys = array_keys($mappings);
      $is_sequential = ctype_digit(implode('', $keys));
      // Normalize mappings.
      if ($is_sequential) {
        $ext_repo = new PhpRepository();
        $normalized_mappings = [];
        foreach ($mappings as $mime_type) {
          $extensions = $ext_repo->findExtensions($mime_type);
          $normalized_mappings[$mime_type] = current($extensions);
        }
        $this->mimeTypeMappings = $normalized_mappings;
      }
      else {
        $this->mimeTypeMappings = $configuration['mime_mappings'];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Only operate on files that have been saved to the file system.
    if (file_exists($value)) {
      $this->validateFile($value);
    }

    return $value;
  }

  /**
   * Validate and modify the file and path value if necessary.
   *
   * @param string $file_path
   *   The path to the file.
   */
  protected function validateFile(&$file_path) {
    if ($this->mimeTypeIsValid($file_path)) {
      if ($this->extensionIsValid($file_path)) {
        // Mime type is valid, extension is valid, nothing to do.
        return;
      }
      else {
        $type = $this->getFileMimeType($file_path);
        $extension = $this->getExtensionForType($type);
        if (!empty($extension)) {
          $file_path = $this->renameFileWithExtension($file_path, $extension);
          return;
        }
        else {
          throw new MigrateSkipRowException('Could not identify valid extension for mime type.');
        }
      }
    }
    else {
      if ($this->shouldCleanDownload()) {
        $this->fileSystem->delete($file_path);
      }
      throw new MigrateSkipRowException('Invalid mime type associated with this file, skipping.');
    }
  }

  /**
   * Rename file with new extension.
   *
   * @param string $file_path
   *   The current path to the file.
   * @param string $extension
   *   The extension which should be substituted in the passed file path.
   *
   * @return string
   *   The updated path of the file.
   */
  protected function renameFileWithExtension($file_path, $extension) {
    $info = pathinfo($file_path);
    $new_file_path = ($info['dirname'] ? $info['dirname'] . DIRECTORY_SEPARATOR : '')
      . $info['filename']
      . '.'
      . $extension;
    $this->fileSystem->move($file_path, $new_file_path);
    return $new_file_path;
  }

  /**
   * Determine if the mime type for the file is valid.
   *
   * @param string $file_path
   *   The path to the file to check.
   *
   * @return bool
   *   TRUE if the mime type is valid, or FALSE otherwise.
   */
  protected function mimeTypeIsValid($file_path) {
    // Check if the mime types mappings array has been provided, and if it has
    // not then assume that all mime types are valid. Otherwise, determine the
    // valid mime types and compare them against the file's mime type.
    if (!empty($this->configuration['mime_mappings'])) {
      $file_mime_type = mime_content_type($file_path);
      $mappings = $this->configuration['mime_mappings'];
      $keys = array_keys($mappings);
      $values = array_values($mappings);
      $is_sequential = ctype_digit(implode('', $keys));
      $mime_types = $is_sequential ? $values : $keys;
      return in_array($file_mime_type, $mime_types);
    }
    return TRUE;
  }

  /**
   * Getter for cleanDownload property.
   *
   * @return bool
   *   TRUE if the download should be cleaned up, FALSE otherwise.
   */
  protected function shouldCleanDownload() {
    return $this->cleanDownload;
  }

  /**
   * Based on file's mime type, determine if it's extension is valid.
   *
   * @param string $file_path
   *   Path to file.
   *
   * @return bool
   *   TRUE if the file's extension is valid, FALSE otherwise.
   */
  protected function extensionIsValid($file_path) {
    $info = pathinfo($file_path);
    $extension = $info['extension'];
    $mime_type = $this->getFileMimeType($file_path);
    $valid_extensions = $this->getExtensionsForType($mime_type);
    return in_array($extension, $valid_extensions);
  }

  /**
   * Determine the mime type of a file.
   *
   * @param string $file_path
   *   Path to file.
   *
   * @return string
   *   The mime type of the file.
   */
  protected function getFileMimeType($file_path) {
    return mime_content_type($file_path);
  }

  /**
   * Get valid extensions for a given mime type.
   *
   * @param string $type
   *   The mime type to get extensions for.
   *
   * @return array|string[]
   *   Array of valid extensions. Empty array if no valid extensions found.
   */
  protected function getExtensionsForType($type) {
    $ext_repo = new PhpRepository();
    return $ext_repo->findExtensions($type);
  }

  /**
   * Get the preferred, or otherwise valid extension for a given mime type.
   *
   * If an extension is provided in a mime type mapping, that will be used.
   * Otherwise the first extension returned from the mime type's extension
   * list in the repository will be used.
   *
   * If no valid extensions were found for the passed mime type, then FALSE
   * will be returned.
   *
   * @param string $type
   *   The mime type for which to retrieve the extension for.
   *
   * @return false|string
   *   The extension for the type, or FALSE if it could not be determined.
   */
  protected function getExtensionForType($type) {
    if (!empty($this->mimeTypeMappings[$type])) {
      return $this->mimeTypeMappings[$type];
    }
    $extensions = $this->getExtensionsForType($type);
    if (!empty($extensions)) {
      return current($extensions);
    }
    return FALSE;
  }

  /**
   * Get the mime types specified in configuration.
   *
   * @return string[]|false
   *   Array of mime types passed to configuration. FALSE if not specified.
   */
  protected function getMimeTypes() {
    if (!empty($this->mimeTypeMappings)) {
      return array_keys($this->mimeTypeMappings);
    }
    return FALSE;
  }

}
